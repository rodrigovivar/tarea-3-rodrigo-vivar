import ddf.minim.*;// libreria de audio

Minim minim;
AudioPlayer p1; // audio parte uno
AudioPlayer p2;// audio parte dos
AudioPlayer p3;// audio parte tres
AudioPlayer p4;// audio parte cuatro

float a=0;//variable para la temperatura
float b=0;//variable para la presión
int MaxI=2;
int imageI=0;

PImage[] images = new PImage[MaxI];
PImage exp;
void setup(){
size (400,400);//tamaño de la ventanilla
textSize(12);// definimos el tamaño de la letra
smooth();
for(int i =0; i<images.length; i++){  //cargar imagentes  en loop
  images[i]=loadImage("I"+ i+".png");
  images[i].resize(100,100);
}
frameRate(8);//fotogramas por segundo
imageMode(CENTER);//imagen centrada
exp=loadImage("exp.png");//cargar explosion
minim=new Minim(this);//crear nueva variable minim
p1=minim.loadFile("1.mp3");//cargas pista 1
p2=minim.loadFile("2.mp3");//cargas pista 2
p3=minim.loadFile("3.mp3");//cargas pista 3
p4=minim.loadFile("4.mp3");//cargas pista 4
}
void draw (){
  background(155);//fondo
  fill(0);//color ciculos
  stroke(255);//borde cirulos
  ellipse (100,100, 50,50); //circulo donde esta ubicado el boton 1
  ellipse (300,100, 50,50);//circulo donde esta ubicado el boton 1
 
//primero definimos los parametros de nuestro primer boton
    //si esta dentro del ciculo 1
        //primera instancia
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a<10)){//
    fill(5, 252, 15);//color
    ellipse (100,200,100,100);
    a+=0.5;//sumar al valor de a
}
      //segunda  instancia
  else
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a<20)){
    fill(74, 252, 5);
    ellipse (100,200,100,100);
    a+=0.5;
  }
        //tercera instancia
  else
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a<40)){
    fill(177, 252, 5);
    ellipse (100,200,100,100);
    a+=1;
  }
        //cuarta instancia
  else
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a<=80)){
    fill(252, 252, 5);
    ellipse (100,200,100,100);
    a+=2;

  }
        //instancia de advertencia
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a>80)){
    fill(252,158,5);
    ellipse (100,200,100,100);
    p2.pause();
    p3.pause();
    p4.pause();
    p1.play();
    a+=1;
  }
        //instancia de peligro 
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a>100)){
    fill(252,75, 5);
    ellipse (100,200,a,a);
    fill(0);
    ellipse (100,100, 50,50);
    p1.pause();
    p3.pause();
    p4.pause();
    p2.play();
    a+=2.5;
  }
        //saturacion
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a>400)){
    fill(252,38,5);
    ellipse (100,200,a,a);
    fill(0);
    ellipse (100,100, 50,50);
    image(images[imageI],100,200);
    imageI=(imageI+1)% images.length;
    p1.pause();
    p2.pause();
    p4.pause();
    p3.play();
    a+=0.05;

  }
        //explosion
  if ((dist(100, 100, mouseX, mouseY)<25)&&(a>490)){
    fill(252,13,5);
    ellipse (100,200,a,a);
    fill(0);
    ellipse (100,100, 50,50);
    image(exp,200,200);
    p1.pause();
    p2.pause();
    p3.pause();
    p4.play();
    a+=1;

  }
  //si se encuentra fuera del segundo boton entonces:
    //saturacion
  else
  if ((dist(100, 100, mouseX, mouseY)>25)&&(a>400)){ //distancia mayor a 25px
    fill(252,38,5);
    ellipse (100,200,a,a);
    fill(0);
    ellipse (100,100, 50,50);
    image(images[imageI],100,200);
    imageI=(imageI+1)% images.length;
    p1.pause();
    p2.pause();
    p4.pause();
    p3.play();
    a-=2;


  }
        //instancia de peligro 
  else
    if ((dist(100, 100, mouseX, mouseY)>25)&&(a<=400)&&(a>100)){
    fill(252,75, 5);
    ellipse (100,200,a,a);
    fill(0);
    ellipse (100,100, 50,50);

    p1.pause();
    p3.pause();
    p4.pause();
    p2.play();
    a-=7.5;  

}
        //instancia de advertencia
  else
    if ((dist(100, 100, mouseX, mouseY)>25)&&(a<=100)&&(a>80)){
    fill(252,158,5);
    ellipse (100,200,100,100);
    p2.pause();
    p3.pause();
    p4.pause();
    p1.play();
    a-=0.5;  

}
        //cuarta instancia
  else
  if ((dist(100, 100, mouseX, mouseY)>25)&&(a<=80)&&(a>40)){
    fill(252, 252, 5);
    ellipse (100,200,100,100);
    p1.pause();
    p2.pause();
    p3.pause();
    p4.pause();
    a-=2;

    
  }
          //tercera instancia
  else
  if ((dist(100, 100, mouseX, mouseY)>25)&&(a<=40)&&(a>20)){
    fill(177, 252, 5);
    ellipse (100,200,100,100);
    a-=1;

  }
          //segunda instancia
  else
  if ((dist(100, 100, mouseX, mouseY)>25)&&(a<=20)&&(a>10)){
    fill(74, 252, 5);
    noStroke();
    ellipse (100,200,100,100);
    a-=0.5;

  }
  else
          //primera instancia
  if ((dist(100, 100, mouseX, mouseY)>25)&&(a<=10)){
    fill(5, 252, 15);
    noStroke();
    ellipse (100,200,100,100);
    a-=0.5; 
  }
  
  
  
  
  
  //en segunda instancia definimos los parametros de nuestro segundo boton(presion)
    //si esta dentro del ciculo 2
    
        //primera instancia
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b<=1)){
    fill(5, 252, 15);//color
    ellipse (300,200,100,100);
    b+=(0.25);//sumar al valor de a
}
      //segunda  instancia
  else
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b<=2)){
    fill(74, 252, 5);
    ellipse (300,200,100,100);
    b+=0.05;
  }
        //tercera instancia
  else
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b<=4)){
    fill(177, 252, 5);
    ellipse (300,200,100,100);
    b+=0.1;
  }
        //cuarta instancia
  else
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b<=8)){
    fill(252, 252, 5);
    ellipse (300,200,100,100);
    b+=0.2;

  }
        //instancia de advertencia
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b>8)){
    fill(252,158,5);
    ellipse (300,200,100,100);
    p2.pause();
    p3.pause();
    p4.pause();
    p1.play();
    b+=0.1;
  }
        //instancia de peligro 
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b>10)){
    fill(252,75, 5);
    ellipse (300,200,(b*15),(b*15));
    fill(0);
    ellipse (300,100, 50,50);
    p1.pause();
    p3.pause();
    p4.pause();
    p2.play();
    b+=0.2;
  }
        //saturacion
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b>20)){
    fill(252,38,5);
    ellipse (300,200,(b*15),(b*15));
    fill(0);
    ellipse (300,100, 50,50);
    image(images[imageI],300,200);//mostrar adventencia
    imageI=(imageI+1)% images.length;//itineracion
    p1.pause();
    p2.pause();
    p4.pause();
    p3.play();
    b+=0.05;

  }
        //explosion
  if ((dist(300, 100, mouseX, mouseY)<25)&&(b>29)){
    fill(252,13,5);
    ellipse (300,200,(b*15),(b*15));
    fill(0);
    ellipse (300,100, 50,50);
    image(exp,200,200);//mostrar explosion
    p1.pause();
    p2.pause();
    p3.pause();
    p4.play();
    b+=0.1;

  }
  //si esta fuera del segundo boton
  
      //saturacion
  else
  if ((dist(300, 100, mouseX, mouseY)>25)&&(b>20)){
    fill(252,38,5);
    ellipse (300,200,(b*15),(b*15));
    fill(0);
    ellipse (300,100, 50,50);
    image(images[imageI],300,200);//mostrar imagen
    imageI=(imageI+1)% images.length;//itineracion de imagenes
    p1.pause();
    p2.pause();
    p4.pause();
    p3.play();
    b-=0.1;


  }
         //instancia de peligro 
  else
    if ((dist(300, 100, mouseX, mouseY)>25)&&(b<=30)&&(b>10)){
    fill(252,75, 5);
    ellipse (300,200,(b*15),(b*15));
    fill(0);
    ellipse (300,100, 50,50);

    p1.pause();
    p3.pause();
    p4.pause();
    p2.play();
    b-=0.25;  

}
        //instancia de advertensia
  else
    if ((dist(300, 100, mouseX, mouseY)>25)&&(b<=10)&&(b>8)){
    fill(252,158,5);
    ellipse (300,200,100,100);
    p2.pause();
    p3.pause();
    p4.pause();
    p1.play();
    b-=0.05;  

}
        //cuarta instancia
  else
  if ((dist(300, 100, mouseX, mouseY)>25)&&(b<=8)&&(b>4)){
    fill(252, 252, 5);
    ellipse (300,200,100,100);
    p1.pause();
    p2.pause();
    p3.pause();
    p4.pause();
    b-=0.2;

    
  }
          //tercera instancia
  else
  if ((dist(300, 100, mouseX, mouseY)>25)&&(b<=4)&&(b>2)){
    fill(177, 252, 5);
    ellipse (300,200,100,100);
    b-=0.1;

  }
          //segunda instancia
  else
  if ((dist(300, 100, mouseX, mouseY)>25)&&(b<=(2))&&(b>(1))){
    fill(74, 252, 5);
    ellipse (300,200,100,100);
    b-=0.05;

  }
          //primera instancia
  else
  if ((dist(300, 100, mouseX, mouseY)>25)&&(b<=(1))){
    fill(5, 252, 15);
    ellipse (300,200,100,100);
    b-=0.25; 
  }
  fill(255);
  rect(40,35,100,20);
  rect(240,35,100,20);
  a = constrain(a,0,500);//parametros de a
  b = constrain(b,0,30);//parametros de b
  delay(20);//tiempo en milisegundo
  fill(0);// definimos el color de la fuente
  text("temperatura", 50, 20);//texto "temperatura"
  text("presión", 250, 20);//texto ""presion"
  String x= a +"°C";
  text(x, 50, 50);//mostrar temperatura
  String y= b +"Bar";//mostrar presion
  text(y, 250, 50);
  fill(255,0,0);//color texto
  text("B1", 95, 105); //texto boton1
  text("B2", 295, 105);//texto boton2
}
